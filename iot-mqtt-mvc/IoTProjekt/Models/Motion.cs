﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IoTProjekt.Models
{
    public class Motion
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public bool Value { get; set; }

        public DateTime InsertedAt { get; set; }
    }
}


