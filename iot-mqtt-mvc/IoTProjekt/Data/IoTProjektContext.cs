﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using IoTProjekt.Models;

namespace IoTProjekt.Data
{
    public class IoTProjektContext : DbContext
    {
        public IoTProjektContext (DbContextOptions<IoTProjektContext> options)
            : base(options)
        {
        }

        public DbSet<IoTProjekt.Models.Motion> Motion { get; set; }
    }
}
