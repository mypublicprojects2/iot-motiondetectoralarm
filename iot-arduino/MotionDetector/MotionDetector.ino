/* Example code to create an alarm system with HC-SR501 PIR motion sensor, buzzer and Arduino. More info: www.makerguides.com */
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Buzzer.h>
// Define connection pins:
#define buzzerPin 5
#define pirPin 16
#define ledPin 13

const char* ssid = "Optima-30153a"; // Enter your WiFi name
const char* password =  "sitek251921"; // Enter WiFi password
const char* mqttServer = "mqtt.vub.zone";
const int mqttPort = 1883;
const char* mqttUser = "iot-ws-dev";
const char* mqttPassword = "M9W7SAWrg_JZ";

WiFiClient espClient;
PubSubClient client(espClient);

// Create variables:
int val = 0;
bool motionState = false; // We start with no motion detected.

char received_topic[128];
byte received_payload[128];
unsigned int received_length;
bool received_msg = false;

void setup() {
  // Configure the pins as input or output:
  pinMode(buzzerPin, OUTPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(pirPin, INPUT);

  // Begin serial communication at a baud rate of 9600:
  Serial.begin(9600);

   WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println("Connected to the WiFi network");
 
  client.setServer(mqttServer, mqttPort);
 
  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");
 
    if (client.connect("ESP8266Client", mqttUser, mqttPassword )) {
 
      Serial.println("connected");  
 
    } else {
 
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
    }
  }
  client.setCallback(callback);
  client.subscribe("iot/asitek/control");
}

void loop() {
  // Read out the pirPin and store as val:
  val = digitalRead(pirPin);
  client.loop();
  // If motion is detected (pirPin = HIGH), do the following:
  if (val == HIGH) {
    alarm(500, 1000);  // Call the alarm(duration, frequency) function.
    delay(150);
     
    // Change the motion state to true (motion detected):
    if (motionState == false) {
      Serial.println("Motion detected!");
      motionState = true;
      String movement = String("true");
      char buf[20];
      movement.toCharArray(buf, 20);
      client.publish("iot/asitek/motion", buf ); //Topic name
    }
  }

  // If no motion is detected (pirPin = LOW), do the following:
  else {
    noTone(buzzerPin); // Make sure no tone is played when no motion is detected.
    delay(150);

    // Change the motion state to false (no motion):
    if (motionState == true) {
      Serial.println("Motion ended!");
      motionState = false;
      String movement = String("false");
      char buf[20];
      movement.toCharArray(buf, 20);
      client.publish("iot/asitek/motion", buf ); //Topic name
    }
  }
}

// Function to create a tone with parameters duration and frequency:
void alarm(long duration, int freq) {
  tone(buzzerPin, freq);
  delay(duration);
  noTone(buzzerPin);
}

void callback(char* topic, byte* payload, unsigned int length) {
  strcpy(received_topic, topic);
  memcpy(received_payload, payload, length);
  received_msg = true;
  received_length = length;
  
  payload[length] = '\0';
  
  String topicStr = topic;
  String message = (char*)payload;

  if(received_msg){
    if(message == "on"){
      digitalWrite(ledPin, LOW); // Turn on the on-board LED. 
    }else{
      digitalWrite(ledPin, HIGH); // Turn off the on-board LED.
    }
  }
}
