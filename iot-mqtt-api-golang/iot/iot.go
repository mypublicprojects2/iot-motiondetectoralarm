package iot

import (
	"encoding/json"
	"fmt"
	"iot-mqtt-api/db"
	"iot-mqtt-api/logger"
	"iot-mqtt-api/mqtt"
	"log"
	"net/http"
	"strconv"
)

type IOT struct {
	database *db.Database
	mqtt     *mqtt.MQTT
	log      *logger.Logger
	data     Data
}

type Data struct {
	Motion bool `json:"motion"`
}

func NewIOT(d *db.Database, m *mqtt.MQTT, l *logger.Logger) *IOT {
	return &IOT{database: d, mqtt: m, log: l}
}

func (iot *IOT) ReceiveData() {
	for {
		message := <-iot.mqtt.Messages

		switch message.Topic() {
		case "iot/asitek/motion":
			iot.data.Motion, _ = strconv.ParseBool(string(message.Payload()))
			iot.insertMotionInDB(iot.data.Motion)
		}
		fmt.Printf("%s - %s\n", message.Topic(), message.Payload())
	}
}

func (iot *IOT) WebServer() {
	http.HandleFunc("/asitek", iot.getData)
	http.HandleFunc("/asitek/control/on", iot.setControlOn)
	http.HandleFunc("/asitek/control/off", iot.setControlOff)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func (iot *IOT) getData(w http.ResponseWriter, r *http.Request) {
	jsonData, _ := json.Marshal(iot.data)
	fmt.Fprintf(w, "%s", jsonData)
}

func (iot *IOT) setControlOn(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	iot.mqtt.Publish("iot/asitek/control", "on")
	w.WriteHeader(200)
	fmt.Println("Control set: on")
}

func (iot *IOT) setControlOff(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	iot.mqtt.Publish("iot/asitek/control", "off")
	w.WriteHeader(200)
	fmt.Println("Control set: off")
}

func (iot *IOT) insertTemperatureInDB(temperature float64) {
	_, err := iot.database.Conn.Exec("INSERT INTO Temperatures (Value, InsertedAt) VALUES ($1,CURRENT_TIMESTAMP)", temperature)
	if err != nil {
		log.Println(err)
	}
}

func (iot *IOT) insertMotionInDB(motion bool) {
	_, err := iot.database.Conn.Exec("INSERT INTO Motion (Value, InsertedAt) VALUES ($1, CURRENT_TIMESTAMP)", motion)
	if err != nil {
		log.Println(err)
	}
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}
