module iot-mqtt-api

go 1.15

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/denisenkom/go-mssqldb v0.9.0
	github.com/eclipse/paho.mqtt.golang v1.3.1
)
